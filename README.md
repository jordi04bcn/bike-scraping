# Bike Scraping

## Installation
* Create virtualenv:
```
python3 -m venv venv
source venv/bin/activate
pip install -r scripts/requirements.txt
```

* Set env variables:
```
export SENDER_EMAIL=sender@email.com
export RECEIVER_USERS=$(cat scripts/receiver-users-example.json)
```

* Set env variables to be used in the pipeline:
  * `GCP_CREDENTIALS` --> User credentials
  * `GCP_TOKEN` --> User token
  * `UPDATE_GITLAB_VARS_TOKEN` --> Gitlab user token

* Set env variables to be used in local:
```
export IS_LOCAL=true
```

* Exit virtualenv:
```
deactivate
```

## Execution
* Generate report:
```
make scrape
```
* Send report:
```
make send_report
```
